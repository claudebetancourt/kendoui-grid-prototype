const fs = require("fs");

/**
 * Removes fonts included with the UICL since this app already includes them.
 * This eliminates over 600KB of unnecessary bloat from the final build.
 */
const path = "./build/static/media";
const prefix = "core-sans-c";
console.log(`Removing UICL fonts named "${prefix}*"...`);
fs.readdir(path, (err, files) => {
  if (err) throw err;
  files.forEach(file => {
    if (file.startsWith(prefix)) {
      fs.unlink(`${path}/${file}`, err => {
        if (err) {
          console.log(`Could not remove ${file}. ${err.message}`);
          return;
        }
        console.log(`removed ${file}`);
      });
    }
  });
});
