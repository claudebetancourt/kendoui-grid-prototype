# KendoUI Grid Prototype ~~React Application Starter Kit~~ #

This is a Single-Page Application built with
[Create React App](https://facebook.github.io/create-react-app/) ~~and components
from the UI Corporate Library (**UICL**)~~.
It borrows architectural patterns introduced by the UICL's boilerplates, which
are under continual development. Development and runtime dependencies are
managed with [NPM](https://www.npmjs.com/package/npm).

~~User authentication is backed by the corporate Customer Identity Access Management service.(**CIAM**)~~

## Requirements ##
*As of 26-Mar-2019*

### IDE: Visual Studio Code ###
This project has been pre-configured with Microsoft's
[Visual Studio Code](https://code.visualstudio.com/) in mind. VSC is an IDE
optimized for modern web development. **VSC v1.32.3** is currently supported.

A number of recommended extensions to aid in the development and debugging of
the application are linked with the source code. Visual Studio Code will prompt
you to install these extensions the first time you open this project.

### Command Line Tools ###
Install the following tools to be able to clone, build and test this project:
* Install **[Git](https://git-scm.com/downloads) v2.20.x** or higher.
* Install the following dependencies using the appropriate method for your operating system:
  * **[NodeJS](https://nodejs.org/) v10.15.3 LTS**.
  * **[NPM](https://www.npmjs.com/package/npm) v6.4.1** - already bundled with node.
  * **[Live Server](https://www.npmjs.com/package/live-server) v1.2.1** globally.

## Getting Started with Development ##

### Get the Source Code ###
* Clone (or download and unzip) this repo.
* From the command line, switch into the project's root directory.

### Install Dependencies ###
Remaining on the command line, run `npm install` to install the project's build
and development dependencies.

### Start Visual Studio Code
* Open the project's root directory in Visual Studio Code.
* If this is your first time opening this project, VSC should prompt you to
review and install the recommended extensions.

### Configure Authentication (optional) ###
If you have a CIAM Client ID, update the following constants:
  * `src/constants.js`: Set the value of `CLIENT_ID` to the Client ID assigned
  to your application by the CIAM team.
  * `src/constants.js`: Set the value of `APPLICATION_ID` to the CIAM
  application ID found in the Identity token. You may have to sign-in at least once to retrieve this value.

If you are planning to register your application with CIAM, use the following
URIs in your initial application profile request:
  * **Redirect URI**: `https://localhost:*/auth`
  * **Audience URI**: This should be your eventual production URL, for example, `https://myapp.changehealthcare.com/`
  * **Callback URI**: Same value as *Audience URI*.

### Launch in Development Mode ###
From the command line, run `npm start` to start the application in development
mode. When successful, your default browser will open automatically to
https://localhost:3000/.

Hit `CTRL + C` to terminate the development session.

### Create a Production Build ###
* Build the application by running `npm run build`.
* A production-ready version of the application will be created in the `./build`
directory.

You can view this build with a static file server like Live Server,
`live-server ./build --port=9801` or by deploying it to a static web server
like Apache, nginx, S3, etc.

### Potential Issues ###
If you experience problems installing the project's build and development
dependencies, chances are you may have older versions installed. To solve this
issue, clean your `npm` cache by running `npm cache clean` and reinstalling the
project dependencies.