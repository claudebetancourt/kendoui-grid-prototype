import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 1.5em 18px;
`;

const PaddedDetailHeader = props => {
  return (
    <Wrapper>
      <p>Report Header</p>
    </Wrapper>
  );
};

export default PaddedDetailHeader;
