/**
 * See https://reactjs.org/docs/error-boundaries.html
 */

import React from "react";
import SimplePage from "./SimplePage";
import Layout from "./layouts/Centered";

const MainContent = ({ error }) => (
  <SimplePage title="An Error Has Occurred" text={error && error.toString()} />
);

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    const { hasError = false, error = null, errorInfo = null } = props;

    this.state = { hasError, error, errorInfo };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ hasError: true, error, errorInfo });
  }

  render() {
    const { hasError, error } = this.state;

    if (hasError) {
      // You can render any custom fallback UI
      return (
        <Layout
          MainContent={MainContent}
          mainContentProps={{ error }}
          padMainContent={true}
        />
      );
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
