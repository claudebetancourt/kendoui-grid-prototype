import React from "react";
import PropTypes from "prop-types";
import { VALID_SIZES } from "./constants";
import { sizeToLength } from "./helpers";

const WrappedLoadingIndicator = ({ size }) => {
  const length = sizeToLength(size);

  return <p>{length}</p>;
};

WrappedLoadingIndicator.propTypes = {
  size: PropTypes.oneOf(VALID_SIZES)
};

WrappedLoadingIndicator.defaultProps = {
  size: "medium"
};

export default WrappedLoadingIndicator;
