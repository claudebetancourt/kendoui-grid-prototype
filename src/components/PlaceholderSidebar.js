import React from "react";

const PlaceholderLeftStrip = () => (
  <div title="Placeholder Sidebar">
    <p className="text-muted">
      Your current layout requires a sidebar component. This placeholder will
      appear until you create it and pass to the layout.
    </p>
  </div>
);

export default PlaceholderLeftStrip;
