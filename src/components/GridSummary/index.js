import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  background-color: #f1f2f3;
  padding: 1.5rem 18px;
  border-top: 4px solid #9ba1a9;
  border-bottom: 1px solid #bec4c6;
  margin-bottom: 0.75rem;
  color: #bec4c6;
  font-weight: 300;
`;

const Summary = () => (
  <Wrapper>
    <p>Grid summary goes here...</p>
  </Wrapper>
);

export default Summary;
