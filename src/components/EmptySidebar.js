import React from "react";
import { LeftStrip } from "ui-core";

const EmptySidebar = ({ title = "Empty Sidebar" }) => (
  <LeftStrip title={title} />
);

export default EmptySidebar;
