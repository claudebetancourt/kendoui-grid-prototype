import styled from "styled-components";

const SimpleWrapper = styled.div`
  padding: 1.5rem 18px;
`;

export default SimpleWrapper;
