import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import EmptyComponent from "../EmptyComponent";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  &.padded {
    padding: 1.5rem 18px;
  }
`;

const Layout = ({ MainContent, mainContentProps, padMainContent }) => {
  const classNames = [];
  padMainContent && classNames.push("padded");

  return (
    <Wrapper className={classNames.join(" ")}>
      <MainContent {...mainContentProps} />
    </Wrapper>
  );
};

Layout.propTypes = {
  MainContent: PropTypes.elementType,
  mainContentProps: PropTypes.object,
  padMainContent: PropTypes.bool
};

Layout.defaultProps = {
  MainContent: EmptyComponent,
  mainContentProps: {},
  padMainContent: false
};

export default Layout;
