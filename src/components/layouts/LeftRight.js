import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import EmptyComponent from "../EmptyComponent";
import PlaceholderSidebar from "../PlaceholderSidebar";

const LayoutWrapper = styled.div`
  .right {
    margin-left: 320px;

    &.padded {
      padding: 1.5rem 18px;
    }
  }
`;

const Layout = ({
  Sidebar,
  MainContent,
  padMainContent,
  sidebarProps,
  mainContentProps
}) => {
  const classNames = ["right"];
  padMainContent && classNames.push("padded");

  return (
    <LayoutWrapper>
      <Sidebar {...sidebarProps} />
      <div className={classNames.join(" ")}>
        <MainContent {...mainContentProps} />
      </div>
    </LayoutWrapper>
  );
};

Layout.propTypes = {
  Sidebar: PropTypes.elementType,
  MainContent: PropTypes.elementType,
  padMainContent: PropTypes.bool,
  sidebarProps: PropTypes.object,
  mainContentProps: PropTypes.object
};

Layout.defaultProps = {
  Sidebar: PlaceholderSidebar,
  MainContent: EmptyComponent,
  padMainContent: false,
  sidebarProps: {},
  mainContentProps: {}
};

export default Layout;
