import React, { useState } from "react";
import PropTypes from "prop-types";
import { Input } from "ui-core";
import ButtonImage from "./components/ButtonImage";

const SearchBox = ({ domID, onButtonClick, onChange, placeholder }) => {
  const [focused, setFocused] = useState(false);
  const onFocus = () => setFocused(true);
  const onBlur = () => setFocused(false);

  const className = focused ? "active" : "";
  const searchIcon = require("./images/search-icon-grey.png");

  return (
    <Input
      domID={domID ? domID : null}
      placeholder={placeholder}
      onButtonClick={onButtonClick}
      onChange={onChange}
      buttonContent={<ButtonImage className={className} src={searchIcon} />}
      onFocus={onFocus}
      onBlur={onBlur}
    />
  );
};

SearchBox.propTypes = {
  domId: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  onButtonClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

SearchBox.defaultProps = {
  domId: "search-input",
  placeholder: "Search",
  onButtonClick: () => false,
  onChange: () => false
};

export default SearchBox;
