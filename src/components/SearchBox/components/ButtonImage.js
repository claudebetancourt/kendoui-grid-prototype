import styled from "styled-components";

const ButtonImage = styled.img`
  width: 16px;
  height: 16px;
  margin-right: 8px;
  opacity: 0.3;

  &.active,
  &:hover {
    opacity: 0.9;
  }

  transition: opacity 400ms;
`;

export default ButtonImage;
