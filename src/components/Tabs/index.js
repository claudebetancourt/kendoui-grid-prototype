import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Wrapper = styled.div`
  &.with-margin {
    margin-bottom: 1.5rem;
  }
`;

const WrappedTabs = ({ withMargin, ...props }) => {
  const { tabs } = props;
  const classNames = [];
  withMargin && classNames.push("with-margin");

  return tabs.length ? (
    <Wrapper className={classNames.join(" ")}>
      <p>Report Tabs</p>
    </Wrapper>
  ) : null;
};

WrappedTabs.propTypes = {
  withMargin: PropTypes.bool
};

WrappedTabs.defaultProps = {
  withMargin: true
};

export default WrappedTabs;
