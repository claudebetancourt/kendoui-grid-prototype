import React from "react";

const Page = ({ title, text }) => (
  <React.Fragment>
    <h1>{title}</h1>
    <p>{text}</p>
  </React.Fragment>
);

export default Page;
