import React from "react";
import { Route, Switch } from "react-router-dom";
import AsyncNotFound from "./containers/NotFound/Loadable";

import AsyncSampleGrid from "./containers/SampleGrid/Loadable";

const Routes = () => (
  <Switch>
    <Route path="/" component={AsyncSampleGrid} />
    <Route component={AsyncNotFound} />
  </Switch>
);

export default Routes;
