/**
 * this side effect sets the column format
 */
import { useState, useEffect } from "react";
import { COLUMN_FORMATS } from "../modules/constants";

const types = Object.keys(COLUMN_FORMATS);

const useColumnFormats = columnConfig => {
  const [columns, setColumns] = useState(columnConfig);

  useEffect(() => {
    columnConfig
      // only process visible columns of a supported type
      .filter(col => col.visible && types.includes(col.type))
      .map(col => (col.format = findFormat(col)));

    setColumns(columnConfig);
  }, [columnConfig]);

  return columns;
};

const findFormat = ({ type, dataName, text }) => {
  // get text matching rules for column type
  const rules = COLUMN_FORMATS[type] || [];

  for (const rule of rules) {
    if (
      // this should be converted to a regex if more keywords are added
      dataName.toLowerCase().includes(rule.keyword) ||
      text.toLowerCase().includes(rule.keyword)
    ) {
      return rule.format;
    }
  }

  return null;
};

export default useColumnFormats;
