import React from "react";
import PropTypes from "prop-types";
import Layout from "../../../components/layouts/LeftRight";
import Sidebar from "./Sidebar";
import MainContent from "./MainContent";

const SampleGrid = ({
  header,
  tabsConfig,
  onTabSelected,
  onRowSelected,
  columnConfig,
  columnSort,
  onColumnSort,
  facets,
  records,
  setOpenedFilters,
  setActiveFilters
}) => {
  const gridConfig = {
    columnConfig,
    columnSort,
    onColumnSort,
    records,
    onRowSelected
  };
  const sidebarProps = { facets, setOpenedFilters, setActiveFilters };
  const mainContentProps = {
    header,
    tabsConfig,
    onTabSelected,
    gridConfig
  };

  return (
    <Layout
      Sidebar={Sidebar}
      MainContent={MainContent}
      sidebarProps={sidebarProps}
      mainContentProps={mainContentProps}
    />
  );
};

SampleGrid.propTypes = {
  header: PropTypes.object.isRequired,
  tabsConfig: PropTypes.object.isRequired,
  onTabSelected: PropTypes.func.isRequired,
  onRowSelected: PropTypes.func.isRequired,
  columnConfig: PropTypes.array.isRequired,
  columnSort: PropTypes.object.isRequired,
  facets: PropTypes.object.isRequired,
  records: PropTypes.array.isRequired,
  setOpenedFilters: PropTypes.func.isRequired,
  setActiveFilters: PropTypes.func.isRequired
};

export default SampleGrid;
