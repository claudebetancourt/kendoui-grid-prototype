import React, { useState } from "react";
import PropTypes from "prop-types";
import { Grid, GridColumn as Column } from "@progress/kendo-react-grid";
import { orderBy } from "@progress/kendo-data-query";
import { INITIAL_SORT_CONFIG } from "./constants";
import style from "./style";

const KendoGrid = ({ records, onRowSelected }) => {
  // fixed sort for demo purposes
  // TODO bubble up to redux state
  const [sortConfig, setSortConfig] = useState(INITIAL_SORT_CONFIG);

  const onSortChange = ({ sort }) =>
    setSortConfig({
      ...INITIAL_SORT_CONFIG,
      sort
    });

  const onRowClick = e => {
    onRowSelected(e.dataItem.id);
  };

  return (
    <Grid
      resizable
      reorderable
      selectedField="selected"
      data={orderBy(records, sortConfig.sort)}
      sortable={{ mode: sortConfig.mode }}
      sort={sortConfig.sort}
      onSortChange={onSortChange}
      onRowClick={onRowClick}
      style={style}
    >
      <Column field="id" title="ID" width="80px" />
      <Column field="benefitYear" title="Benefit Year" width="120px" />
      <Column field="asOfMonth" title="As of Month" width="120px" />
      <Column field="memberNumber" title="Member Number" width="200px" />
      <Column field="lastName" title="Last Name" width="200px" />
      <Column field="firstName" title="First Name" width="200px" />
      <Column
        field="totalEligibleMonths"
        format="{0:n2}"
        title="Eligible Mo/Year"
        width="120px"
      />
      <Column
        field="currentScore"
        format="{0:n3}"
        title="Current Score"
        width="120px"
      />
      <Column
        field="acceptedScore"
        format="{0:n3}"
        title="Accepted Score"
        width="120px"
      />
      <Column
        field="expectedScore"
        format="{0:n3}"
        title="Potential Score"
        width="120px"
      />
      <Column
        field="currentScoreDiff"
        format="{0:n3}"
        title="Current Score Difference"
        width="120px"
      />
      <Column
        field="acceptedScoreDiff"
        format="{0:n3}"
        title="Accepted Score Difference"
        width="120px"
      />
      <Column field="csiId" title="CSI" width="80px" />
      <Column field="csiv" title="CSI Variable" width="120px" />
      <Column field="metalLevel" title="Metal Level" width="120px" />
      <Column field="ageBand" title="Age Band" width="120px" />
      <Column field="gender" title="Gender" width="120px" />
      <Column
        field="totalClaimPaidAmount"
        format="{0:c2}"
        title="Total Claim Payment Amount"
        width="200px"
      />
      <Column
        field="medicalClaimPaidAmount"
        format="{0:c2}"
        title="Medical Claim Payment Amount"
        width="200px"
      />
      <Column
        field="pharmacyClaimPaidAmount"
        format="{0:c2}"
        title="Pharmacy Claim Payment Amount"
        width="200px"
      />
      <Column field="commPlanId" title="Commercial Plan ID" width="200px" />
      <Column field="hiosId" title="HIOS ID" width="120px" />
      <Column field="planidcnt" title="# Plan IDs" width="120px" />
      <Column field="marketType" title="Plan Market" width="120px" />
      <Column field="memberState" title="Member State" width="120px" />
      <Column field="planState" title="Plan State" width="120px" />
      <Column field="planRatingAreaId" title="Plan Rating Area" width="120px" />
      <Column field="subsidyStatus" title="Subsidy Status" width="120px" />
      <Column
        field="subscriberRelationshipType"
        title="Relationship to Primary Subscriber"
        width="200px"
      />
      <Column
        field="pcpProviderNbr"
        title="PCP Provider Number"
        width="200px"
      />
      <Column field="pcpFirstName" title="PCP First Name" width="200px" />
      <Column field="pcpLastName" title="PCP Last Name" width="200px" />
      <Column
        field="memberGroupNbr"
        title="Member Group Number"
        width="200px"
      />
      <Column field="activeStatus" title="Active Status" width="120px" />
      <Column field="exchangeType" title="Exchange Type" width="200px" />
      <Column
        field="nativeAmericanStatus"
        title="Native American Status"
        width="120px"
      />
    </Grid>
  );
};

KendoGrid.propTypes = {
  records: PropTypes.array.isRequired
};

export default KendoGrid;
