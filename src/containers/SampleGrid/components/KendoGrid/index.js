import React from "react";
import PropTypes from "prop-types";
import { Grid, GridColumn as Column } from "@progress/kendo-react-grid";
import { orderBy } from "@progress/kendo-data-query";
import useColumnFormats from "../../hooks/useColumnFormats";
import { COLUMN_MIN_RESIZABLE_WIDTH } from "./constants";
import style from "./style";

const KendoGrid = ({
  columnConfig,
  records,
  columnSort,
  onColumnSort,
  onRowSelected
}) => {
  // column formatting hook
  useColumnFormats(columnConfig);

  // handle Kendo sort event and passes sort config to redux action
  const onSortChange = ({ sort }) => {
    onColumnSort(sort);
  };

  // handle Kendo row selection event and passes row ID to redux action
  const onRowClick = e => {
    onRowSelected(e.dataItem.id);
  };

  return (
    <Grid
      resizable
      reorderable
      selectedField="selected"
      data={orderBy(records, columnSort.sort)}
      sortable={{ mode: columnSort.mode }}
      sort={columnSort.sort}
      onSortChange={onSortChange}
      onRowClick={onRowClick}
      style={style}
    >
      {columnConfig
        .filter(col => col.visible)
        .map(({ dataName, text, sortable, width = 140, format }) => (
          <Column
            key={dataName}
            field={dataName}
            title={text}
            sortable={sortable}
            format={format}
            width={width}
            minResizableWidth={COLUMN_MIN_RESIZABLE_WIDTH}
          />
        ))}
    </Grid>
  );
};

KendoGrid.propTypes = {
  records: PropTypes.array.isRequired
};

export default KendoGrid;
