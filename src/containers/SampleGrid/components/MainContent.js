import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import PaddedDetailHeader from "../../../components/PaddedDetailHeader";
import GridSummary from "../../../components/GridSummary";
import GridTabs from "../../../components/Tabs";
import Grid from "./KendoGrid";

const GridOverflowWrapper = styled.div`
  overflow: hidden;
  overflow-x: auto;
`;

const MainContent = ({ header, tabsConfig, onTabSelected, gridConfig }) => (
  <React.Fragment>
    <PaddedDetailHeader {...header} />
    <GridSummary />
    <GridTabs {...tabsConfig} onTabSelect={onTabSelected} />
    <GridOverflowWrapper>
      <Grid {...gridConfig} />
    </GridOverflowWrapper>
  </React.Fragment>
);

MainContent.propTypes = {
  header: PropTypes.object.isRequired,
  gridConfig: PropTypes.object.isRequired
};

export default MainContent;
