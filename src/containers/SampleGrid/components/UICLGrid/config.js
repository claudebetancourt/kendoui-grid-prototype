const domID = "rvc-all-members";

const selectionKey = "id";

const config = {
  // hide these controls when there are no records
  supportSelection: false,
  isConfigurable: false,

  // TODO Leaving out this prop results in "grid-wrapper-null"
  domID,

  /**
   * TODO This prop should be renamed rowKey or something to indicate its
   * purpose. The prefix "selection" is misleading since it's required even
   * when supportSelection is false.
   */
  selectionKey,

  columns: new Set(),
  // columnOptions: [],

  /**
   * TODO Not sure what this is for, but there is a fat exception when it's
   * left out.
   */
  isDragOver: ""

  // selectedItemIds: new Set([]),
  // bookmarkedItemIds: new Set([]),
  // sharingShown: false,
  // onColDragStart: () => false,
  // onColDragOver: () => false,
  // onColDragDropBefore: () => false,
  // onColDragDropAfter: () => false,
  // onColDrag: () => false,
  // onColDrop: () => false,
  // onSortTableColumn: () => false,
  // onRowSelect: () => false,
  // onSelectAll: () => false,
  // onRowBookmark: () => false,
  // onClickThrough: () => false,
  // onSettingsCancel: () => false,
  // onSharingCancel: () => false,
  // toggleTableColumn: () => false,
  // settingsTakeoverOnRestoreDefaults: () => false,
  // settingsTakeoverOnModalToggle: () => false,
  // settingsTakeoverOnSave: () => false
};

export default config;
