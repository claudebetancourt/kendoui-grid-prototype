import React from "react";
import PropTypes from "prop-types";
import { Grid } from "ui-core";
import config from "./config";

const Component = ({ columnConfig, records }) => {
  const hasData = !!records.length;
  return (
    <Grid
      {...config}
      columns={new Set(columnConfig)}
      records={records}
      supportSelection={hasData}
      isConfigurable={hasData}
    />
  );
};

Component.propTypes = {
  columnConfig: PropTypes.array.isRequired,
  records: PropTypes.array.isRequired
};

export default Component;
