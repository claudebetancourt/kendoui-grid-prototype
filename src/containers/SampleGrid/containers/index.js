import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AllMembers from "../components";
import {
  asyncTabsFetch,
  asyncColumnsFetch,
  asyncDataFetch,
  setOpenedFilters,
  setActiveFilters,
  updateRecordCount,
  selectTab,
  selectRow,
  sortColumns
} from "../modules/actions";
import {
  selectHeader,
  selectTabsConfig,
  selectColumnConfig,
  selectRecords,
  selectFacets,
  selectColumnSortConfig
} from "../modules/selectors";

const mapStateToProps = ({ sampleGrid }) => ({
  header: selectHeader(sampleGrid),
  tabsConfig: selectTabsConfig(sampleGrid),
  columnConfig: selectColumnConfig(sampleGrid),
  columnSort: selectColumnSortConfig(sampleGrid),
  records: selectRecords(sampleGrid),
  facets: selectFacets(sampleGrid)
});

// dispatchable action creators
const actionCreators = {
  asyncTabsFetch,
  asyncColumnsFetch,
  asyncDataFetch,
  setOpenedFilters,
  setActiveFilters,
  updateRecordCount,
  onTabSelected: (event, data) => selectTab(data.selectedTabIndex),
  onRowSelected: id => selectRow(id),
  onColumnSort: sort => sortColumns(sort)
};

const AllMembersContainer = ({
  asyncTabsFetch,
  asyncColumnsFetch,
  asyncDataFetch,
  updateRecordCount,
  header,
  tabsConfig,
  columnConfig,
  columnSort,
  facets,
  records,
  setOpenedFilters,
  setActiveFilters,
  onTabSelected,
  onRowSelected,
  onColumnSort
}) => {
  useEffect(() => {
    asyncTabsFetch();
    asyncColumnsFetch();
    asyncDataFetch();
  }, [asyncColumnsFetch, asyncDataFetch, asyncTabsFetch]);

  useEffect(() => {
    updateRecordCount(records.length);
  }, [records.length, updateRecordCount]);

  return (
    <AllMembers
      header={header}
      tabsConfig={tabsConfig}
      columnConfig={columnConfig}
      columnSort={columnSort}
      facets={facets}
      records={records}
      setOpenedFilters={setOpenedFilters}
      setActiveFilters={setActiveFilters}
      onTabSelected={onTabSelected}
      onRowSelected={onRowSelected}
      onColumnSort={onColumnSort}
    />
  );
};

AllMembersContainer.propTypes = {
  asyncColumnsFetch: PropTypes.func.isRequired,
  asyncDataFetch: PropTypes.func.isRequired,
  updateRecordCount: PropTypes.func.isRequired,
  header: PropTypes.object.isRequired,
  tabsConfig: PropTypes.object.isRequired,
  onTabSelected: PropTypes.func,
  columnConfig: PropTypes.array.isRequired,
  columnSort: PropTypes.object.isRequired,
  facets: PropTypes.object.isRequired,
  records: PropTypes.array.isRequired,
  setOpenedFilters: PropTypes.func.isRequired,
  setActiveFilters: PropTypes.func.isRequired
};

AllMembersContainer.defaultProps = {
  onTabSelected: () => false
};

export default connect(
  mapStateToProps,
  actionCreators
)(AllMembersContainer);
