import { createSelector } from "reselect";

export const selectHeader = state => state.header;
export const selectTabsConfig = state => state.tabsConfig;
export const selectColumnConfig = state => state.columns;
export const selectColumnSortConfig = state => state.columnSort;
export const selectFilters = state => state.filters;
export const selectFiltersMenu = state => state.filtersMenu;

const selectSelectedRowId = state => state.selectedRowId;
const selectRawData = state => state.data;

export const selectSomeRawData = createSelector(
  [selectRawData],
  data => data.slice(0, 30)
);

export const selectVisibleGridColumns = createSelector(
  [selectColumnConfig],
  columns => columns.filter(column => column.visible)
);

const selectFilterableGridColumns = createSelector(
  [selectColumnConfig],
  columns => columns.filter(column => column.filterable)
);

export const selectRecords = createSelector(
  [selectRawData, selectFilters, selectSelectedRowId],
  (records, filters, selectedRowId) => {
    // apply filters when defined
    for (const filter of filters) {
      const [key, set] = filter;
      records = records.filter(record => set.has(record[key]));
    }

    // apply selected row
    records = records.map(record => {
      record.selected = record.id === selectedRowId ? true : false;
      return record;
    });

    return records;
  }
);

export const selectFacets = createSelector(
  [
    selectFilterableGridColumns,
    selectRecords,
    selectFilters,
    selectFiltersMenu
  ],
  (filterableColumns, records, activeFilters, openGroupIDs) => {
    // build a facet config for each filterable column
    const facets = filterableColumns.map(({ dataName, text }) => {
      // create unique set of values for each filterable column
      const values = new Set(records.map(row => row[dataName]));
      return {
        id: dataName,
        text,
        items: [...values].sort().map(value => ({
          id: `${dataName}:${value}`,
          text: value
        }))
      };
    });

    // create deferred state properties
    const selectedIDs = new Set();
    activeFilters.forEach((values, key) => {
      values.forEach(value => {
        selectedIDs.add(`${key}:${value}`);
      });
    });

    const response = {
      // only return facets with selectable data
      items: facets.filter(facet => facet.items.length),
      deferState: {
        openGroupIDs,
        selectedIDs
      }
    };

    return response;
  }
);
