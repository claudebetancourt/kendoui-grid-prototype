export const INITIAL_STATE = {
  header: {
    category: "Risk View Commercial",
    title: "All Members",
    description: "As of May 2018, latest benefit year",
    count: 0
  },
  tabsConfig: {
    tabs: [],
    initialTab: 2,
    onTabSelect: () => false
  },
  columns: [],
  data: [],
  columnSort: {
    mode: "multiple",
    sort: [
      {
        dir: "desc",
        field: "totalEligibleMonths"
      },
      {
        dir: "asc",
        field: "currentScore"
      }
    ]
  },
  selectedRowId: 94,
  filters: new Map(),
  filtersMenu: new Set()
};

/**
 * Used by the useColumnFormats hook to make an educated guess about
 * a column's formatting.
 */
export const COLUMN_FORMATS = {
  number: [
    {
      keyword: "score",
      format: "{0:n3}"
    },
    {
      keyword: "amount",
      format: "{0:c2}"
    },
    {
      keyword: "month",
      format: "{0:n2}"
    }
  ]
};

export const TABS_URL = "/stubs/all-members-tabs.json";
export const COLUMNS_URL = "/stubs/all-members-columns.json";
export const DATA_URL = "/stubs/rvc-all-members-flat.json";

export const FETCH_TABS_SUCCESS = "FETCH_TABS_SUCCESS";
export const FETCH_TABS_FAILURE = "FETCH_TABS_FAILURE";
export const FETCH_DATA_SUCCESS = "FETCH_DATA_SUCCESS";
export const FETCH_DATA_FAILURE = "FETCH_DATA_FAILURE";
export const FETCH_COLUMNS_SUCCESS = "FETCH_COLUMNS_SUCCESS";
export const FETCH_COLUMNS_FAILURE = "FETCH_COLUMNS_FAILURE";
export const FILTERS_MENU_TOGGLED = "FILTERS_MENU_TOGGLED";
export const FILTER_SELECTED = "FILTER_SELECTED";
export const RECORD_COUNT_CHANGED = "RECORD_COUNT_CHANGED";
export const TAB_SELECTED = "TAB_SELECTED";
export const ROW_SELECTED = "ROW_SELECTED";
export const COLUMNS_SORTED = "COLUMNS_SORTED";
