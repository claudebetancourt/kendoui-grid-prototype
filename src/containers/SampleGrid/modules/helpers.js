import { valueFormatter } from "../../../utilities";

/**
 * Transforms a Risk View Commercial dataset into Grid records
 * @param {Array} columnConfig
 * @param {Array} data
 */
export const transformDataToGridRecords = (columnConfig = [], data = []) =>
  data.map(row => {
    let newRow = {};
    columnConfig.forEach(column => {
      const { dataName, type } = column;
      newRow[dataName] = valueFormatter(type, row[dataName].value);
    });
    return newRow;
  });

export const mapSelectedFilters = filters => {
  const kvMap = filters.map(filter => splitFilterString(filter));

  // create set of keys
  const keySet = new Set(kvMap.map(({ key }) => key));

  // create final map
  const map = new Map();

  // convert set to array...
  [...keySet]
    .sort()
    // add keys to final map
    .forEach(key => map.set(key, new Set()));

  // add selected values to final map
  kvMap.forEach(({ key, value }) => {
    map.has(key) && map.get(key).add(value);
  });

  return map;
};

const splitFilterString = (filter, sep = ":") => {
  if ("string" === typeof filter) {
    const [key, value] = filter.split(sep);
    return { key, value };
  }
  return;
};
