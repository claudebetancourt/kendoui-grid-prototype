import {
  makeRequest,
  makeActionCreator,
  validateList
} from "../../../utilities";
import gridColumnSchema from "../../../schemas/grid/column";
import tabSchema from "../../../schemas/navigation/tab";
import {
  TABS_URL,
  COLUMNS_URL,
  DATA_URL,
  FETCH_TABS_SUCCESS,
  FETCH_TABS_FAILURE,
  FETCH_COLUMNS_SUCCESS,
  FETCH_COLUMNS_FAILURE,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_FAILURE,
  FILTERS_MENU_TOGGLED,
  FILTER_SELECTED,
  RECORD_COUNT_CHANGED,
  TAB_SELECTED,
  ROW_SELECTED,
  COLUMNS_SORTED
} from "./constants";

export const asyncTabsFetch = () => {
  const request = {
    url: TABS_URL
  };
  return async dispatch => {
    try {
      const response = await makeRequest(request);
      const tabs = validateList(response, tabSchema);
      return dispatch(fetchTabsSuccess(tabs));
    } catch (err) {
      return dispatch(fetchTabsFailure(err));
    }
  };
};

export const asyncColumnsFetch = () => {
  const request = {
    url: COLUMNS_URL
  };
  return async dispatch => {
    try {
      const response = await makeRequest(request);
      const columns = validateList(response, gridColumnSchema);
      return dispatch(fetchColumnsSuccess(columns));
    } catch (err) {
      return dispatch(fetchColumnsFailure(err));
    }
  };
};

export const asyncDataFetch = () => {
  const request = {
    url: DATA_URL
  };
  return async dispatch => {
    try {
      const response = await makeRequest(request);
      return dispatch(fetchDataSuccess(response));
    } catch (err) {
      return dispatch(fetchDataFailure(err));
    }
  };
};

const fetchTabsSuccess = makeActionCreator(FETCH_TABS_SUCCESS, "payload");
const fetchTabsFailure = makeActionCreator(FETCH_TABS_FAILURE, "error");
const fetchColumnsSuccess = makeActionCreator(FETCH_COLUMNS_SUCCESS, "payload");
const fetchColumnsFailure = makeActionCreator(FETCH_COLUMNS_FAILURE, "error");
const fetchDataSuccess = makeActionCreator(FETCH_DATA_SUCCESS, "payload");
const fetchDataFailure = makeActionCreator(FETCH_DATA_FAILURE, "error");

// expects a list of opened menu filters
export const setOpenedFilters = makeActionCreator(
  FILTERS_MENU_TOGGLED,
  "payload"
);

// expects a list of selected filters
export const setActiveFilters = makeActionCreator(FILTER_SELECTED, "payload");

export const updateRecordCount = makeActionCreator(
  RECORD_COUNT_CHANGED,
  "count"
);

export const selectTab = makeActionCreator(TAB_SELECTED, "tabIndex");
export const selectRow = makeActionCreator(ROW_SELECTED, "id");
export const sortColumns = makeActionCreator(COLUMNS_SORTED, "payload");
