import { APPLICATION_NAME, CURRENT_VERSION } from "../../../constants";

export const SET_USER = "SET_USER";
export const STATUS_UPDATE = "STATUS_UPDATE";
export const ENABLE_NOTIFICATIONS = "ENABLE_NOTIFICATIONS";
export const UNSET_USER = "UNSET_USER";

export const DEFAULT_LOGO_REDIRECT_PATH = "/";
export const CURRENT_PRODUCT_NAME = APPLICATION_NAME;
export const META_LABEL = CURRENT_VERSION;
export const SHOW_NOTIFICATIONS = true;

const DEFAULT_NAV_MENU_ITEMS = [
  [{ id: 0, label: "Home", path: "/" }],
  [
    { id: 0, label: "Usage Instructions", path: "/usage" },
    { id: 1, label: "Sample Grid", path: "/sample-grid" },
    { id: 2, label: "Missing Page", path: "/i-dont-exist" }
  ]
];

export const NAV_MENU_ITEMS = [...DEFAULT_NAV_MENU_ITEMS];

export const PRODUCT_MENU_ITEMS = [];
export const ON_PRODUCT_MENU_OPEN_CLOSE = () => false;

export const AVATAR_MENU_ITEMS = [
  [
    { id: 0, label: "My Profile", path: "/user/profile" },
    { id: 1, label: "My Views", path: "/user/views" }
  ],
  [{ id: 2, label: "Log Out", path: "/logout" }]
];

export const ON_SUPPORT_MENU_SEARCH = () => false;

export const INITIAL_STATE = {
  mastheadConfig: {
    userName: null,
    userEmail: null,
    avatarMenuItems: [],
    navMenuItems: DEFAULT_NAV_MENU_ITEMS,
    logoRedirect: DEFAULT_LOGO_REDIRECT_PATH,
    currentProductName: CURRENT_PRODUCT_NAME,
    productMenuItems: [],
    onProductMenuOpenClose: ON_PRODUCT_MENU_OPEN_CLOSE,
    metaLabel: META_LABEL,
    showNotifications: false,
    supportMenuSearch: ON_SUPPORT_MENU_SEARCH
  }
};
