import { makeActionCreator } from "../../../utilities";
import {
  SET_USER,
  UNSET_USER,
  STATUS_UPDATE,
  ENABLE_NOTIFICATIONS
} from "./constants";

export const setUser = makeActionCreator(SET_USER, "payload");
export const unsetUser = makeActionCreator(UNSET_USER);
export const statusUpdate = makeActionCreator(STATUS_UPDATE, "payload");
export const enableNotifications = makeActionCreator(
  ENABLE_NOTIFICATIONS,
  "payload"
);
