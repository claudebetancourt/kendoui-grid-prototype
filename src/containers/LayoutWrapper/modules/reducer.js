import produce from "immer";
import {
  INITIAL_STATE,
  SET_USER,
  UNSET_USER,
  STATUS_UPDATE,
  ENABLE_NOTIFICATIONS,
  NAV_MENU_ITEMS,
  AVATAR_MENU_ITEMS,
  SHOW_NOTIFICATIONS
} from "./constants";
import { getProductMenuFromApps } from "./helpers";

const reducer = (state = INITIAL_STATE, action) => {
  // return early if an action is not passed
  if (!action) return state;

  switch (action.type) {
    case SET_USER:
      const { apps, email, given_name, family_name } = action.payload;
      const productMenuItems = getProductMenuFromApps(apps);
      return produce(state, draft => {
        draft.mastheadConfig.userEmail = email;
        draft.mastheadConfig.userName = `${given_name} ${family_name}`;
        draft.mastheadConfig.navMenuItems = NAV_MENU_ITEMS;
        draft.mastheadConfig.avatarMenuItems = AVATAR_MENU_ITEMS;
        draft.mastheadConfig.productMenuItems = productMenuItems;
        draft.mastheadConfig.showNotifications = SHOW_NOTIFICATIONS;
      });

    case UNSET_USER:
      return INITIAL_STATE;

    case STATUS_UPDATE:
      return produce(state, draft => {
        draft.mastheadConfig.metaLabel = String(action.payload);
      });

    case ENABLE_NOTIFICATIONS:
      return produce(state, draft => {
        draft.mastheadConfig.showNotifications = Boolean(action.payload);
      });

    default:
      return state;
  }
};

export default reducer;
