import React from "react";
import PropTypes from "prop-types";

const LayoutWrapper = ({
  mastheadConfig: config,
  navMenuSelected,
  children
}) => (
  <div className="app-wrapper">
    <p>Masthead</p>
    <div id="app-content">{React.Children.toArray(children)}</div>
  </div>
);

LayoutWrapper.propTypes = {
  children: PropTypes.node,
  mastheadConfig: PropTypes.object.isRequired
};

export default LayoutWrapper;
