import React from "react";
import { connect } from "react-redux";
import LayoutWrapper from "../components";

const mapStateToProps = ({ layout }) => layout;

// dispatchable action creators
const actionCreators = {};

const LayoutWrapperContainer = props => {
  return <LayoutWrapper {...props} />;
};

export default connect(
  mapStateToProps,
  actionCreators
)(LayoutWrapperContainer);
