import Loadable from "react-loadable";
import EmptyLoadingIndicator from "../../components/EmptyLoadingIndicator";

const LoadableComponent = Loadable({
  loader: () => import("./index"),
  loading: EmptyLoadingIndicator
});

export default LoadableComponent;
