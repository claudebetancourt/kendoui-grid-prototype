import React from "react";
import Layout from "../../../components/layouts/Centered";
import MainContent from "./MainContent";

const NotFound = () => (
  <Layout MainContent={MainContent} padMainContent={true} />
);

export default NotFound;
