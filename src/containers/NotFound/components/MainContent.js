import React from "react";
import SimplePage from "../../../components/SimplePage";

const text =
  "We could not find the page you requested. If this issue persists, please contact a system administrator.";

const MainContent = () => <SimplePage title="Page Not Found" text={text} />;

export default MainContent;
