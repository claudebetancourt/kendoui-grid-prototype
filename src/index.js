import "@babel/polyfill";

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import configureStore, { historyAPI } from "./configureStore";
import App from "./App";
// import registerServiceWorker from "./registerServiceWorker";

import "./styles/global.scss";

// create redux store
const store = configureStore();

const MOUNT_NODE = document.getElementById("root");

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={historyAPI}>
      <App />
    </ConnectedRouter>
  </Provider>,
  MOUNT_NODE
);

// registerServiceWorker();
