import pkg from "../package.json";

export const AUTH_CONFIG = {
  CLIENT_ID: null,
  APPLICATION_ID: null
};

// how early to show warning before session token expires
export const AUTH_MINUTES_BEFORE_SESSION_EXPIRES = 5;

export const AUTHENTICATED_REQUESTS = {
  INCLUDE_IDENTITY: false
};

export const WEB_STORAGE_CONFIG = {
  namespace: "react-app-starter-kit"
};

export const APPLICATION_NAME = pkg.description;
export const CURRENT_VERSION = `Version ${pkg.version}`;
export const INITIAL_STATE = {};
export const USE_LOGGER = false;

export const DELAY_SPEED_FAST = 200;
export const DELAY_SPEED_NORMAL = 400;
export const DELAY_SPEED_SLOW = 800;
export const DELAY_SPEED_SLOWEST = 1600;
