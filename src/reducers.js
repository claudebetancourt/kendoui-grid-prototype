import { combineReducers } from "redux";
import layout from "./containers/LayoutWrapper/modules/reducer";
import sampleGrid from "./containers/SampleGrid/modules/reducer";

export default combineReducers({
  layout,
  sampleGrid
});
