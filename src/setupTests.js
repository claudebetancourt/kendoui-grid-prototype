/**
 * Setup shallow rendering tests with enzyme
 * https://facebook.github.io/create-react-app/docs/running-tests
 */

import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import "jest-enzyme";

configure({
  adapter: new Adapter()
});
