/**
 * Implements Yup schema validation helpers.
 * See https://www.npmjs.com/package/yup
 */

import { VALIDATOR_OPTIONS } from "./constants";

/**
 * Validates a single object against the passed in Yup schema
 *
 * @param {Object} item to be validated
 * @param {ObjectSchema} schema instance to be applied
 * @returns {boolean} valid
 */
export const validateItem = (item, schema) => {
  try {
    return schema.validateSync(item, VALIDATOR_OPTIONS);
  } catch (err) {
    console.error(err);
    return false;
  }
};

/**
 * Validates a dataset against the passed in Yup schema and returns all valid records.
 *
 * @param {Array} list to be validated
 * @param {ObjectSchema} schema instance to be applied
 * @returns {Array} list of objects that pass validation
 */
export const validateList = (list, schema) =>
  list.map(item => validateItem(item, schema)).filter(item => item);

/**
 * Attempts to convert a value to the given type, if supported.
 * Unsupported types are returned unchanged.
 *
 * @param {String} type, a supported data type.
 * @param {Any} value
 * @returns {Any} value
 */
export const valueFormatter = (type, value) => {
  switch (type) {
    case "number":
      return Number(value);
    case "date":
      return Date.parse(value);
    case "boolean":
      return Boolean(value);
    default:
      return value;
  }
};
