export const VALID_DATA_TYPES = ["string", "number", "date", "boolean"];
export const VALIDATOR_OPTIONS = {
  stripUnknown: true
};
