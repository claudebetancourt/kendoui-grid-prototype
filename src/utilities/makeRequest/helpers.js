import { DEFAULT_OPTIONS, DEFAULT_HEADERS } from "./constants";

export const mergeOptions = (options, authenticated) => {
  let opts = { ...DEFAULT_OPTIONS, ...options };
  opts = {
    ...opts,
    headers: {
      ...getHeaders(authenticated)
    }
  };

  return opts;
};

/**
 * Returns a list of default and authentication headers when required
 *
 * @param {Boolean} authenticated
 * @returns {Object} headers
 */
const getHeaders = (authenticated = false) => {
  let headers = {
    ...DEFAULT_HEADERS
  };

  if (authenticated) {
    headers = { ...headers, ...getAuthenticationHeaders() };
  }

  return headers;
};

/**
 * Returns a list of authentication headers
 *
 * @param {Boolean} includeIdentity whether to include the Identify header
 * @returns {Object} authentication headers
 */
const getAuthenticationHeaders = () => ({});

/**
 * Checks if a network request came back fine, and throws an error if not.
 *
 * @param {Object} response
 * @returns {Object|undefined} either the response, or throws an error
 */
export const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
};

/**
 * Parses the JSON returned by a network request or null for
 * status codes without payload.
 *
 * @param {Object} network response
 * @returns {Object} parsed JSON
 */
export const parseJSON = response => {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
};

/**
 * Serializes an object into a query string
 *
 * @param {Object} params
 * @returns {String} query string
 */
export const toQueryString = params => {
  return Object.keys(params)
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
    .join("&");
};
