import { fetch } from "whatwg-fetch";
import { toQueryString, mergeOptions, checkStatus, parseJSON } from "./helpers";

/**
 * Wraps fetch requests with smart defaults, accepts custom options and query
 * parameters. It also includes authentication headers as needed.
 *
 * @param {Object} config
 */
export const makeRequest = ({
  url,
  params = null,
  options = {},
  authenticated = true
} = {}) => {
  let URL = url;
  if (params) {
    URL = `${URL}?${toQueryString(params)}`;
  }

  const OPTIONS = mergeOptions(options, authenticated);

  return fetch(URL, OPTIONS)
    .then(checkStatus)
    .then(parseJSON);
};
