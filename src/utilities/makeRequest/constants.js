export const DEFAULT_OPTIONS = {
  credentials: "omit"
};

export const DEFAULT_HEADERS = {
  Accept: "application/json",
  "Content-Type": "application/json"
};
