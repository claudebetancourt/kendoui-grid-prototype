import uuid from "uuidv4";

export const create = () => uuid();
export const createFromString = value => uuid.fromString(value);
export const createEmpty = () => uuid.empty();
export const verify = value => uuid.is(value);
