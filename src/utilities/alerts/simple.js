/**
 * Implements a Sweetalert2 simple alert.
 *
 * Returns a Promise.
 */

import Alert from "sweetalert2";
import "./style.scss";

const simpleAlert = ({
  title = "Oops!",
  text = "Something went wrong!",
  html = null,
  type = "error",
  confirmButtonText = "OK",
  width = 600,
  animation = true
} = {}) =>
  Alert.fire({
    title,
    text,
    html,
    type,
    confirmButtonText,
    width,
    animation,
    allowOutsideClick: false,
    backdrop: `
				rgba(0, 0, 0, .75)
			`
  });

export { simpleAlert as default };
