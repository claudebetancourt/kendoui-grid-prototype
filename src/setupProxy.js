/**
 * Manually configures the proxies used during development.
 * See https://facebook.github.io/create-react-app/docs/proxying-api-requests-in-development
 */

const proxy = require("http-proxy-middleware");

const aService = proxy("/aServicePath", {
  target: "http://localhost:8080"
});

const anotherService = proxy("/anotherServicePath", {
  target: "http://localhost:8888"
});

module.exports = app => {
  app.use(aService);
  app.use(anotherService);
};
