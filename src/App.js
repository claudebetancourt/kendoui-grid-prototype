import React from "react";
import ErrorBoundary from "./components/ErrorBoundary";
import AsyncLayoutWrapper from "./containers/LayoutWrapper/Loadable";
import Routes from "./Routes";

const App = () => (
  <AsyncLayoutWrapper>
    <ErrorBoundary>
      <Routes />
    </ErrorBoundary>
  </AsyncLayoutWrapper>
);

export default App;
