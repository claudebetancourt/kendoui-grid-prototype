/**
 * Models a tab to be used with the UICL Tabs component
 */

import { object, string } from "yup";
import { createUUID } from "../../../utilities";

const schema = object({
  label: string()
    .required()
    .trim(),
  domID: string()
    .default(() => createUUID())
    .trim(),
  icon: string()
    .nullable()
    .default(null)
    .trim()
});

export default schema;
