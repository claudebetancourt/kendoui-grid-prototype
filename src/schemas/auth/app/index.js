/**
 * Models a CIAM app descriptor object, typically found in the identity token
 */

import { object, string } from "yup";
import { verifyUUID } from "../../../utilities";

const schema = object({
  id: string()
    .required()
    .trim()
    .test("uuid", "id must be a UUID", verifyUUID),
  name: string()
    .required()
    .trim(),
  uri: string()
    .required()
    .trim()
});

export default schema;
