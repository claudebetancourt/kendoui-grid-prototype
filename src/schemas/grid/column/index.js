/**
 * Models a grid column
 */

import { object, string, boolean } from "yup";
import { VALID_DATA_TYPES } from "../../../utilities/schema/constants";

const schema = object({
  dataName: string()
    .required()
    .trim(),
  text: string()
    .required()
    .trim(),
  type: string()
    .default("string")
    .oneOf(VALID_DATA_TYPES)
    .trim(),
  format: string()
    .nullable()
    .default(null)
    .trim(),
  tooltip: string()
    .nullable()
    .default(null)
    .trim(),
  visible: boolean().default(true),
  sortable: boolean().default(false),
  filterable: boolean().default(false),
  visibilityFrozen: boolean().default(false)
});

export default schema;
