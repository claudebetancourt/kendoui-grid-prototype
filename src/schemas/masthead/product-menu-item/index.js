/**
 * Models a UICL product menu item
 */

import { object, string } from "yup";
import { verifyUUID } from "../../../utilities";

const schema = object({
  id: string()
    .required()
    .trim()
    .test("uuid", "id must be a UUID", verifyUUID),
  path: string()
    .required()
    .trim(),
  label: string()
    .required()
    .trim(),
  labelDecoration: string()
    .nullable()
    .trim()
    .default(null)
});

export default schema;
