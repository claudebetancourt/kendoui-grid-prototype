# 1.8.2 #
Cleaned up grid styles.

# 1.8.1 #
Lifted grid column sorting up to global state.

# 1.8.0 #
Added support for dynamic generation of KendoUI Grid columns.

# 1.7.1 #
- Fixed `/login` and `/logout` redirects in Auth Timeout module.
- Added redirect home when silent refresh of a session fails.

# 1.7.0 #
Added generic components that implement the general design guidance shared by Chris Meeks.

# 1.6.1 #
- Fixed minor bug.
- Updated README with suggested new CIAM request details.

# 1.6.0 #
Added support for session expiration notification and background token refresh for logged in users.

This feature is enabled automatically when the integration with CIAM is enabled. See the "Configure Authentication" section of the [README](./README.md) for more details.

# 1.5.3 #
- Added `react-hooks/exhaustive-deps` to ESLint rules. See more about this [lint rule here](https://github.com/facebook/react/issues/14920).
- Improved performance of side effects.

# 1.5.2 #
Fixed missing redux action in main layout wrapper.

# 1.5.1 #
- Replaced lifecycle methods with useEffect hooks. Also converted those classes to functional components.
- Updated README with latest tested version of NodeJS.

# 1.5.0 #
Updated runtime and development dependencies to latest versions.

# 1.4.1 #
Updated the change log.

# 1.4.0 #
Added sample grid.

# 1.3.4 #
Updated babel polyfill rules and `browserslist`.

# 1.3.3 #
Reorganized containers.

# 1.3.2 #
Cleaned up sidebar placeholder components.

# 1.3.1 #
- Added sidebar and main content props to Left-Right layout component.
- Replaced `div` with `main` tag.

# 1.3.0 #
Enhanced Left-Right layout component.

# 1.2.1 #
Fixed entry routes.

# 1.2.0 #
Cleaned up Left-Right layout component.

# 1.1.0 #
Added release version to masthead and updated grid column schema.

# 1.0.3 #
Updated tests.

# 1.0.2 #
Updated `react-scripts` to version 2.1.8.

# 1.0.1 #
Updated tests and README.

# 1.0.0 #
Initial release.
